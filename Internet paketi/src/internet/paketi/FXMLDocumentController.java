
package internet.paketi;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;


public class FXMLDocumentController implements Initializable {
    ObservableList lista=FXCollections.observableArrayList();
     ObservableList lista2=FXCollections.observableArrayList();
      ObservableList lista3=FXCollections.observableArrayList();
    @FXML
    private ChoiceBox protok;
   @FXML
  private TableView TableView;
@FXML
private ChoiceBox brzina;
    @FXML
    private Label label;
    @FXML
    private Button button;
  
    @FXML
    private ChoiceBox ugovor;
   

    
    @FXML
 private TextField ugovor1;
    
    
 @FXML
private TextField brzina1;
 @FXML
 private TextField protok1;
    
 
 
 @FXML private TextField ime1;
    @FXML private TextField prezime1;
    @FXML private TextField id1;
    @FXML private TextField adresa1;
     @FXML private TableColumn<Person1,String> ime;
      @FXML private TableColumn<Person1,String> prezime;
       @FXML private TableColumn<Person1,String> id;
        @FXML private TableColumn<Person1,String> adresa;
  
 
        
     @FXML
    private void handleButtonAction(ActionEvent event) {
      Person1 person = new Person1(ime1.getText(),prezime1.getText(),id1.getText(),adresa1.getText()
             );
       
        TableView.getItems().add(person);
        
        
        
   String ug = (String) ugovor.getValue();
    if(ug!=null){
       ugovor1.setText(ug);
      
    String br = (String) brzina.getValue();
    if(br!=null){
        brzina1.setText(ug);
        
        
        String pt = (String) protok.getValue();
        if(pt!=null){
            protok1.setText(ug);
        }
    }
       
        
    }
        
        
    
      
    }
    
    @FXML
    public void DeleteBTN(){
        ObservableList<Person1>selectedRows,allPeople;
        allPeople=TableView.getItems();
        selectedRows=TableView.getSelectionModel().getSelectedItems();
        for(Person1 p:selectedRows){
            allPeople.remove(p);
            protok1.clear();
            brzina1.clear();
            ugovor1.clear();
            
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
       loadUgovor2();  
      
        loadBrzina2();
        
        loadProtok2();
        
        
        
        ime.setCellValueFactory(new PropertyValueFactory<Person1,String>("ime"));
     id.setCellValueFactory(new PropertyValueFactory<Person1,String>("id"));
      prezime.setCellValueFactory(new PropertyValueFactory<Person1,String>("prezime"));
     adresa.setCellValueFactory(new PropertyValueFactory<Person1,String>("adresa"));
  
     
 
   
   TableView.setItems(getPeople());
     
   
     
     
     
    }    

    private ObservableList<Person1> getPeople() {
        ObservableList<Person1>people=FXCollections.observableArrayList();
        return people;
        
    }
   private void loadUgovor2(){
       lista.removeAll(lista);
       String a ="1 godina";
       String b ="2.godine";
       lista.addAll(a,b);
       ugovor.getItems().addAll(lista);
   }
   private void loadBrzina2(){
       lista2.removeAll(lista2);
        String a2 ="2 Mbit";
        
        String b2 ="5 Mbit";        
        String c2 ="10 Mbit";
        String d2 = "20 Mbit";
        String g2 ="50 Mbit";
        String e2 = "100 Mbit";
lista2.addAll(a2,b2,c2,d2,g2,e2);
brzina.getItems().addAll(lista2);
   }
   private void loadProtok2(){
       lista3.remove(lista3);
       String a3="1GB";
         String b3="5GB";       
         String c3="10GB";       
        String d3="100GB";  
        lista3.addAll(a3,b3,c3,d3);
         protok.getItems().addAll(lista3);
               
   }
}